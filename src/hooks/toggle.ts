import { useState } from "react";

const useToggle = (init: boolean): any => {
  const [toggleState, setToggleState] = useState(init);
  const toggle = (force?: boolean) => {
    setToggleState(typeof force !== "undefined" ? force : !toggleState);
  };
  return [toggleState, toggle];
};

export { useToggle };
