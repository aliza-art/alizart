import { useEffect, EffectCallback, DependencyList } from "react";

const useMount = (effect: EffectCallback, deps?: DependencyList): void => {
  useEffect(effect, []);
};

const useUnMount = (effect: EffectCallback) => {
  return useEffect(effect, []);
};

export { useMount, useUnMount };
