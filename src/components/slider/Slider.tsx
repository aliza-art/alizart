import React from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import "./Slider.scss";

const ImgSlider = () => {
  return (
    <div>
      <Carousel
        additionalTransfrom={4}
        arrows
        autoPlaySpeed={3000}
        centerMode={false}
        className="carousel-con"
        containerClass="container-with-dots"
        dotListClass="test tree"
        focusOnSelect={false}
        infinite
        itemClass="test two"
        keyBoardControl
        minimumTouchDrag={80}
        renderButtonGroupOutside={true}
        renderDotsOutside={false}
        responsive={{
          desktop: {
            breakpoint: {
              max: 3000,
              min: 1024,
            },
            items: 1,
            partialVisibilityGutter: 0,
          },
          mobile: {
            breakpoint: {
              max: 464,
              min: 0,
            },
            items: 1,
            partialVisibilityGutter: 50,
            paritialVisibilityGutter: 60,
          },
          tablet: {
            breakpoint: {
              max: 1024,
              min: 464,
            },
            items: 1,
            partialVisibilityGutter: 0,
          },
        }}
        showDots={true}
        sliderClass="test"
        slidesToSlide={1}
        swipeable
      >
        <img alt="" src={`images/homePage/DSC_1_fullSize.jpg`} />
        <img alt="" src={`images/homePage/DSC_2_fullSize.jpg`} />
        <img alt="" src={`images/homePage/DSC_3_fullSize.jpg`} />
        <img alt="" src={`images/homePage/DSC_4_fullSize.jpg`} />
      </Carousel>
    </div>
  );
};

export default ImgSlider;
