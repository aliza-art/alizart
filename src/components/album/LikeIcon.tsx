import classNames from "classnames";
import React, { useEffect, useContext, useState } from "react";
import { app } from "../../firebaseSettings";
import { camelize } from "../../helper/string";
import { AlbumContext, ImageInfo } from "./Album";

const GALLERY_OBJ_NAME = "galleryLikes";

interface LikeProps {
  className: string;
  likes: number;
  id: string;
}

interface userLocalStorage {
  [key: string]: boolean;
}

const LikeIcon = (props: LikeProps) => {
  const { likes, className, id } = props;

  const [likeNumber, setLikeNumber] = useState<number>(likes);
  const [didUserLike, setDidUserLike] = useState<boolean>(false);
  const { name } = useContext(AlbumContext);
  const ImageRef = app.database().ref(camelize(name as any) + `/${id}`);

  useEffect(() => {
    ImageRef.on("value", imageRefCallback);

    var userLocalStorage = getLocalStorage();
    if (userLocalStorage && userLocalStorage[id]) {
      setDidUserLike(true);
    }

    return () => {
      ImageRef.off("value", imageRefCallback);
    };
  }, []);

  useEffect(() => {
    const localStorage = getLocalStorage();
    if (localStorage && localStorage[id] !== didUserLike) {
      setDidUserLike(!!localStorage[id]);
    }
  }, [id]);

  const imageRefCallback = (snapshot: any) => {
    var ImageInfo: ImageInfo = snapshot.val();
    setLikeNumber(ImageInfo.likes);
    const localStorage = getLocalStorage();
    if (localStorage && localStorage[id] !== didUserLike) {
      setDidUserLike(!!localStorage[id]);
    }
  };

  const toggleLike = () => {
    ImageRef.transaction((ImageInfo: ImageInfo) => {
      if (ImageInfo) {
        var ls = getLocalStorage();
        if (didUserLike && ImageInfo.likes > 0) {
          ImageInfo.likes--;
          if (ls && ls[id]) {
            setDidUserLike(false);
            delete ls[id];
          }
          // updateLocalStorage(id, LS, false, true);
        } else {
          ImageInfo.likes++;
          if (!ImageInfo.likes) {
            ImageInfo.likes = 0;
          }
          setDidUserLike(true);
          ls[id] = true;

          // updateLocalStorage(id, LS, true);
        }
        localStorage.setItem(GALLERY_OBJ_NAME, JSON.stringify(ls));
        setLikeNumber(ImageInfo.likes);
      }
      return ImageInfo;
    });
  };

  const getLocalStorage = (): userLocalStorage => {
    const userLS: any = localStorage.getItem(GALLERY_OBJ_NAME);
    return JSON.parse(userLS) || {};
  };

  const likeLogoClass = classNames("like-logo", {
    active: didUserLike,
  });

  return (
    <div role="button" className={className} onClick={toggleLike}>
      <span className="like-number">{likeNumber}</span>
      <svg role="presentation" viewBox="0 0 24 24" className={likeLogoClass}>
        <path d="M12.094 18.563c4.781-4.313 7.922-7.172 7.922-10.078 0-2.016-1.5-3.469-3.516-3.469-1.547 0-3.047 0.984-3.563 2.344h-1.875c-0.516-1.359-2.016-2.344-3.563-2.344-2.016 0-3.516 1.453-3.516 3.469 0 2.906 3.141 5.766 7.922 10.078l0.094 0.094zM16.5 3c3.094 0 5.484 2.391 5.484 5.484 0 3.797-3.375 6.844-8.531 11.531l-1.453 1.313-1.453-1.266c-5.156-4.688-8.531-7.781-8.531-11.578 0-3.094 2.391-5.484 5.484-5.484 1.734 0 3.422 0.844 4.5 2.109 1.078-1.266 2.766-2.109 4.5-2.109z"></path>
      </svg>
    </div>
  );
};

export default LikeIcon;
// const updateLocalStorage = (
//   id: string,
//   ls: any,
//   value: boolean,
//   deleteItem: boolean = false
// ) => {
//   var newLocalStorage = { ...ls };
//   if (deleteItem && ls[id]) {
//     delete newLocalStorage[id];
//   } else {
//     newLocalStorage[id] = value;
//   }
//   localStorage.setItem(GALLERY_OBJ_NAME, JSON.stringify(newLocalStorage));
//   setLS(newLocalStorage);
// };
