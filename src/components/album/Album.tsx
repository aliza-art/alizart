import React, { Dispatch, SetStateAction, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Loader from "react-loader-spinner";
import classNames from "classnames";
import LazyLoad from "react-lazyload";
import Carousel, { Modal, ModalGateway } from "react-images";
import { app } from "../../firebaseSettings";
import Image from "../image/Image";
import { camelize } from "../../helper/string";
import { useToggle } from "../../hooks";
import { CustomHeader, CustomFooterCaption } from "./CustomComponents";
import LikeIcon from "./LikeIcon";
import { albumsMap } from "../gallery/Gallery";
import "./Album.scss";

interface urlParams {
  id: string;
}

export interface ImageInfo {
  likes: number;
  shares: number;
  url: string;
  user: any;
  uploadDay: string;
}

interface ImageInfoObj {
  [key: string]: ImageInfo;
}

interface AlbumContext {
  name: string;
  images?: ImageInfoObj;
  setImages?: Dispatch<SetStateAction<ImageInfoObj>>;
}

export const AlbumContext = React.createContext<AlbumContext>({ name: "" });

const NewAlbum = () => {
  let { id } = useParams<urlParams>();
  const [allImages, setAllImages] = useState<ImageInfoObj>({});
  const [selectedIndex, setSelectedIndex] = useState<number>(0);
  const [lightBoxIsOpen, toggleLightBoxIsOpen] = useToggle(false);

  useEffect(() => {
    const fetchAlbum = async () => {
      app
        .database()
        .ref(camelize(id))
        .once("value")
        .then((allImages) => {
          setAllImages(allImages.val());
        });
    };
    fetchAlbum();
  }, []);

  const handelImgClick = (selectedIndex: number) => {
    toggleLightBoxIsOpen();
    setSelectedIndex(selectedIndex);
  };

  const albumClass = classNames("album", {
    loader: allImages && Object.keys(allImages).length === 0,
  });

  return (
    <>
      <AlbumContext.Provider value={{ name: id }}>
        <div className={albumClass}>
          {albumsMap[id] && (
            <div className="header">{albumsMap[id].headerName}</div>
          )}
          {allImages && Object.keys(allImages).length !== 0 ? (
            <>
              <LazyLoad>
                {Object.entries(allImages).map(([k, v], j) => {
                  return (
                    <div key={k} className="img-like-con">
                      <Image
                        key={k}
                        imageKey={k}
                        imageInfo={v}
                        onClick={handelImgClick}
                        index={j}
                      />
                      <LikeIcon className="icon" likes={v.likes} id={k} />
                    </div>
                  );
                })}
              </LazyLoad>

              <ModalGateway>
                {lightBoxIsOpen ? (
                  <Modal onClose={() => handelImgClick(selectedIndex)}>
                    <Carousel
                      components={{
                        Header: CustomHeader,
                        FooterCaption: CustomFooterCaption,
                      }}
                      currentIndex={selectedIndex}
                      views={Object.entries(allImages).map(([key, value]) => {
                        return {
                          data: value,
                          source: value.url,
                          caption: key,
                        };
                      })}
                      styles={{
                        header: (base) => ({ ...base, opacity: 1 }),
                        footer: (base) => ({ ...base, opacity: 1 }),
                        navigation: (base) => ({ ...base, opacity: 1 }),
                      }}
                    />
                  </Modal>
                ) : null}
              </ModalGateway>
            </>
          ) : (
            <Loader
              type="Circles"
              color="#d3a5e5"
              height={100}
              width={100}
              timeout={3000}
            />
          )}
        </div>
      </AlbumContext.Provider>
    </>
  );
};

export default NewAlbum;

/* <div
                    className="sa"
                    onClick={() => {
                      localStorage.clear();
                    }}
                  >
                    DDDD
                  </div> */

// button.aaaa {
//   position: absolute!important;
//   max-width: 100px;
//   max-height: 100px;
//   right: 50px!important;
//   opacity: 1;
//   top: 50%!important;
//   left: 48%;
//   align-items: center!important;
//   justify-content: center!important;
//   /* color: black!important; */
//   display: flex!important;
// }
