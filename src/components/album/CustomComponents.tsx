import React, { useContext } from "react";
import { CommonProps } from "react-images";
import GetAppIcon from "@material-ui/icons/GetApp";
import FullscreenIcon from "@material-ui/icons/Fullscreen";
import CloseIcon from "@material-ui/icons/Close";
import ShareIcon from "@material-ui/icons/Share";
import LikeIcon from "./LikeIcon";
import { isMobile } from "react-device-detect";
import { AlbumContext, ImageInfo } from "./Album";
import { app } from "../../firebaseSettings";
import { camelize } from "../../helper/string";
import { toast } from "react-toastify";

const CustomHeader = (props: any) => {
  const { modalProps, getStyles, currentView, data } = props;
  const { onClose, toggleFullscreen } = modalProps;
  const { likes } = currentView.data;
  const { name } = useContext(AlbumContext);
  const ImageRef = app.database().ref(camelize(name) + `/${data.caption}`);

  const onDownloadClick = (fileName: string, url: string) => {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.responseType = "blob";

    xhr.onload = function () {
      var urlCreator = window.URL || window.webkitURL;
      var imageUrl = urlCreator.createObjectURL(this.response);
      var tag = document.createElement("a");
      tag.href = imageUrl;
      tag.download = fileName;
      document.body.appendChild(tag);
      tag.click();
      document.body.removeChild(tag);
    };
    xhr.send();
  };

  const handleOnShare = (url: string) => {
    if (navigator.share) {
      navigator
        .share({
          title: "Alizart",
          text: `קבלו ציור קטלני!`,
          url: url,
        })
        .then(() => {
          ImageRef.transaction((ImageInfo: ImageInfo) => {
            if (ImageInfo) {
              ImageInfo.shares++;
            }
            return ImageInfo;
          });
          toast.success("שותף בהצלחה");
        })
        .catch((error: any) => {
          toast.error("אירעה שגיאה במהלך השיתוף");
          console.error("אירעה שגיאה במהלך השיתוף", error);
        });
    } else {
      toast.error("אין אפשרות לשתף ממכשיר זה");
    }
  };

  return (
    <div
      style={{
        ...getStyles("header", props),
        ...{ left: "none", right: "1px", top: "10px" },
      }}
    >
      <div className="icons-con">
        {isMobile ? (
          <ShareIcon
            style={{ fontSize: "25" }}
            fontSize="large"
            className="icon"
            onClick={() => handleOnShare(data.source)}
          />
        ) : null}

        <LikeIcon className="icon" likes={likes} id={data.caption} />

        <GetAppIcon
          fontSize="large"
          {...props}
          className="icon"
          onClick={() => onDownloadClick(data.caption, data.source)}
        />

        <FullscreenIcon
          {...props}
          fontSize="large"
          className="icon"
          onClick={toggleFullscreen}
        />

        <CloseIcon
          fontSize="large"
          {...props}
          className="icon"
          onClick={onClose}
        />
      </div>
    </div>
  );
};

const CustomFooterCaption = (CommonProps: CommonProps) => (
  <div>{CommonProps.currentView?.caption}</div>
);

export { CustomHeader, CustomFooterCaption };
