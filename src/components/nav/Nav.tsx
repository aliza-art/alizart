import React from "react";
import { Link } from "react-router-dom";
import "./Nav.scss";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"; //todo

const LOGO = require("../../../public/icons/logo.png");

const Nav = () => {
  return (
    <div className="nav-con">
      <Link to="/contact-us">צור קשר</Link>
      <Link to="/usersUploads">העלת תמונות</Link>
      <Link to="/albums">גלרייה</Link>
      <Link to="/about">אודות</Link>
      <Link to="/home">
        <div className="logo-con">
          <img className="logo" src={LOGO} alt="בית" />
        </div>
      </Link>
    </div>
  );
};

export default Nav;
