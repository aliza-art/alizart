import React, { useState } from "react";
import {
  Button,
  createMuiTheme,
  FormControl,
  TextareaAutosize,
  TextField,
  ThemeProvider,
} from "@material-ui/core";
import emailjs from "emailjs-com";
import { toast } from "react-toastify";
import "./Contact.scss";

const mailing = require("../../../public/icons/mailing.svg");

const Contact = () => {
  const [name, setName] = useState("");
  const [mail, setMail] = useState("");
  const [contact, setContact] = useState("");

  const submitHandle = (e: any) => {
    e.preventDefault();
    emailjs
      .sendForm(
        "service_2hzf7rr",
        "template_8hw1cjo",
        e.target,
        "user_ujP1DpJ5r76LuQMVKZg6C"
      )
      .then(
        () => {
          setMail("");
          setName("");
          setContact("");
          toast.success("תודה על פנייתך נחזור אלייך בהקדם :) ");
        },
        (error) => {
          toast.error("אירעה שגיאה בעת שליחת המייל אנא נסה שוב:( ");
          console.log(error.text);
        }
      );
  };

  const theme = createMuiTheme({ direction: "rtl" });

  return (
    <div className="contact-us" dir="rtl">
      <div className="head">
        <h1>צור קשר </h1>
        <img alt="מייל" src={mailing} />
      </div>
      <ThemeProvider theme={theme}>
        <FormControl
          onSubmit={submitHandle}
          className="contact-form"
          component="form"
        >
          <TextField
            name="name"
            required
            placeholder="שם"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <TextField
            type="email"
            name="mail"
            size="medium"
            placeholder="מייל"
            value={mail}
            onChange={(e) => setMail(e.target.value)}
          />
          <TextareaAutosize
            name="message"
            placeholder="פנייה"
            value={contact}
            onChange={(e) => setContact(e.target.value)}
          />
          <Button type="submit" variant="contained" color="primary">
            שלח
          </Button>
        </FormControl>
      </ThemeProvider>
    </div>
  );
};

export default Contact;
