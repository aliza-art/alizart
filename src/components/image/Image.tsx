import React, { FC } from "react";
import { ImageInfo } from "../album/Album";
import "./Image.scss";

interface ImageProps {
  onClick: (index: number) => void;
  imageKey: string;
  index: number;
  imageInfo: ImageInfo;
}

const Image: FC<ImageProps> = (props: ImageProps) => {
  const { onClick, imageKey, imageInfo, index } = props;
  return (
    <>
      <img
        alt={imageInfo?.user?.name}
        onClick={() => onClick(index)}
        key={imageKey}
        src={imageInfo.url}
        className="img-con"
      />
    </>
  );
};

export default Image;
