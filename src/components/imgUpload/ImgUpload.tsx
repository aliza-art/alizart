import React, { createRef, useEffect, useState } from "react";
import { app } from "../../firebaseSettings";
import { FilePond, registerPlugin } from "react-filepond";
import "filepond/dist/filepond.min.css";
import * as firebase from "firebase/app";
import FilePondPluginImageExifOrientation from "filepond-plugin-image-exif-orientation";
import FilePondPluginImagePreview from "filepond-plugin-image-preview";
import "filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css";
import {
  FetchServerConfigFunction,
  LoadServerConfigFunction,
  ProcessServerConfigFunction,
  RemoveServerConfigFunction,
  RestoreServerConfigFunction,
  RevertServerConfigFunction,
  ServerUrl,
} from "filepond";
import { Link } from "react-router-dom";
import {
  fullBrowserVersion,
  getUA,
  mobileModel,
  mobileVendor,
  deviceType,
  deviceDetect,
} from "react-device-detect";
import "./ImgUpload.scss";
import {
  Button,
  createMuiTheme,
  TextField,
  ThemeProvider,
} from "@material-ui/core";
import { toast } from "react-toastify";

registerPlugin(FilePondPluginImageExifOrientation, FilePondPluginImagePreview);

var database = firebase.database();
interface settingsProps {
  url?: string;
  timeout?: number;
  headers?: {
    [key: string]: string | boolean | number;
  };
  process?: string | ServerUrl | ProcessServerConfigFunction | null;
  revert?: string | ServerUrl | RevertServerConfigFunction | null;
  restore?: string | ServerUrl | RestoreServerConfigFunction | null;
  load?: string | ServerUrl | LoadServerConfigFunction | null;
  fetch?: string | ServerUrl | FetchServerConfigFunction | null;
  remove?: RemoveServerConfigFunction | null;
}
const UPLOAD_FOLDER = "usersUploads";
const storageRef = app.storage().ref(UPLOAD_FOLDER);

const initName = "unknown";
const ImgUpload = () => {
  const [files, setFiles] = useState<any>([]);
  const [fileUpload, setFileUpload] = useState<Boolean>(false);
  const [name, setName] = useState(initName);
  const [tempName, setTempName] = useState("");

  const fileRef = createRef<FilePond>();

  useEffect(() => {
    const lsName = localStorage.getItem("name");
    if (lsName) setName(lsName);
  }, []);

  const serverSettings = (): settingsProps => {
    var settings: settingsProps = {
      process: (_fieldName, file, _metadata, load, error, progress, _abort) => {
        const id = `${
          file.name.split(".")[0]
        }-${new Date().toLocaleTimeString()}`;
        const fileRef = storageRef.child(id);

        const task = fileRef.put(file);
        // db.collection("gallery").doc(file.name).set({ likes: 0, shares: 0 });

        task.on(
          firebase.storage.TaskEvent.STATE_CHANGED,
          (snap) => {
            // console.log("progress: %o", snap);
            progress(true, snap.bytesTransferred, snap.totalBytes);
          },
          (err) => {
            setFileUpload(false);
            toast.error("אירעה שגיאה בהעלת הקובץ");
            console.log(err);
            error(err.message);
          },
          async () => {
            setFileUpload(true);
            var deviceDetectInfo = { ...deviceDetect() };
            const imgUrl = await fileRef.getDownloadURL();
            database.ref(UPLOAD_FOLDER + "/" + id).set({
              url: imgUrl,
              likes: 0,
              shares: 0,
              uploadDay: new Date().valueOf(),
              user: {
                name,
                fullBrowserVersion,
                mobileModel,
                mobileVendor,
                getUA,
                deviceType,
                ...deviceDetectInfo,
              },
            });
            toast.success("הקובץ עלה בהצלחה");
            load(id);
          }
        );
      },
      load: (source, load, error, progress, abort) => {
        progress(true, 0, 1024);
        storageRef
          .child(source)
          .getDownloadURL()
          .then((url: any) => {
            let xhr = new XMLHttpRequest();
            xhr.responseType = "blob";
            xhr.onload = (event) => {
              let blob = xhr.response;
              load(blob);
            };
            xhr.open("GET", url);
            xhr.send();
          })
          .catch((err: any) => {
            error(err.message);
            abort();
          });
      },

      revert: (source, load, error) => {
        storageRef.child(source).delete();
        toast.error("הקובץ נמחק בהצלחה");

        error("file deleted");
      },
    };

    return settings;
  };

  const onSaveName = () => {
    if (!tempName.length) {
      toast.error("שם לא יכול להיות ריק");
    } else {
      localStorage.setItem("name", tempName);
      setName(tempName);
      toast.success("שם נוסף בהצלחה");
    }
  };
  return (
    <>
      <div className="upload-con" style={{ textAlign: "center" }}>
        {name === initName ? (
          <ThemeProvider theme={createMuiTheme({ direction: "rtl" })}>
            <p>:אנא הכנס\י שם להעלת תמונה</p>
            <div className="user-name" dir="rtl">
              <TextField
                name="name"
                required
                placeholder="שם"
                value={tempName}
                onChange={(e) => setTempName(e.target.value)}
              />
              <Button onClick={onSaveName} variant="contained" color="primary">
                שלח
              </Button>
            </div>
          </ThemeProvider>
        ) : (
          <>
            <div
              className="na"
              title={"לחץ פעמיים לשינוי שם"}
              onDoubleClick={() => setName(initName)}
            >
              היי {name}
            </div>
            <FilePond
              labelFileProcessing="מעלה"
              labelButtonRemoveItem="מחק"
              labelTapToCancel="לחץ לביטול"
              labelTapToUndo="לחץ לביטול"
              labelButtonProcessItem="העלה"
              labelButtonAbortItemProcessing="בטל"
              labelButtonAbortItemLoad="בטל"
              labelFileProcessingComplete="הקובץ הועלה בהצלחה"
              labelIdle={
                'גרור לפה קבצים או  <span class="filepond--label-action"> בחר </span>'
              }
              files={files}
              ref={fileRef}
              maxFiles={333}
              name="files"
              onupdatefiles={setFiles}
              allowMultiple={true}
              acceptedFileTypes={[]}
              server={serverSettings()}
            />

            {fileUpload ? (
              <Link to="/album/usersUploads">
                הקובץ עלה בהצלחה לחץ כאן כדי לראות אותו באלבום
              </Link>
            ) : null}
          </>
        )}
      </div>
    </>
  );
};

export default ImgUpload;

/* <ThemeProvider theme={createMuiTheme({ direction: "rtl" })}>
          <div className="s" dir="rtl">
            <TextField
              name="name"
              required
              placeholder="שם"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
            <Button variant="contained" color="primary">
              שלח
            </Button>
          </div>
        </ThemeProvider> */
