import React from "react";
import Nav from "./nav/Nav";
import HomePage from "./homePage/HomePage";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import About from "./about/About";
import Contact from "./contact/Contact";
import Multimedia from "./multimedia/Multimedia";
import Gallery from "./gallery/Gallery";
import ImgUpload from "./imgUpload/ImgUpload";
import NewAlbum from "./album/Album";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import NewGallery from "./gallery/NewGallery";

export default function MainPage() {
  return (
    <>
      <Router>
        <Nav />
        <Switch>
          <Route path="/gallery">
            <Gallery />
          </Route>
          <Route path="/usersUploads">
            <ImgUpload />
          </Route>
          <Route path="/albums">
            <NewGallery />
          </Route>
          <Route path="/album/:id">
            <NewAlbum />
          </Route>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/contact-us">
            <Contact />
          </Route>
          <Route path="/">
            <HomePage />
          </Route>
        </Switch>
        <Multimedia />
      </Router>
      <ToastContainer
        position="bottom-left"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop
        closeOnClick
        rtl
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </>
  );
}
