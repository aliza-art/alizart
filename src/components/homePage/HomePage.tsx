import React from "react";
import ImgSlider from "../slider/Slider";

const HomePage = () => {
  return (
    <>
      <div className="home-con">
        <ImgSlider />
      </div>
    </>
  );
};
export default HomePage;
