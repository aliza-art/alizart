import React from "react";
import { MAIL } from "../multimedia/Multimedia";
import { Link } from "react-router-dom";
import "./About.scss";

const GRANDMA_IMG = require("../../../public/icons/grandma.jpg");

const About = () => {
  return (
    <div className="about-con">
      <img className="about-img" src={GRANDMA_IMG} alt="הסבתא הכי הכי" />
      <div className="about-text">
        הי שמי עליזה אני גרה במושב קלחים שבדרום ומציירת לכיף מעל 30 שנה. מוזמנים
        להיכנס <Link to="/albums">לגלריה</Link> ולהנות
      </div>
      <div className="about-text">לכל הערה או הארה אני זמינה במייל *</div>
      <a href={`mailto:${MAIL}`}>{MAIL}</a>
    </div>
  );
};

export default About;
