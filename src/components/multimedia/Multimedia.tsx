import React from "react";
import "./Multimedia.scss";
import { TelegramShareButton } from "react-share";
//@ts-ignore
import { FacebookProvider } from "react-facebook";
const MAIL_ICON = require("../../../public/icons/004-mail.svg");
const TELEGRAM_ICON = require("../../../public/icons/003-telegram.svg");
const WHATSAPP_ICON = require("../../../public/icons/001-whatsapp.svg");
const MESSENGER_ICON = require("../../../public/icons/002-messenger.svg");

export const MAIL = "aliza.siboni.art@gmail.com";
const PHONE_NUMBER = "972528821988";
const PAGE_ID = "102388121645027";
const MAIL_SUBJECT = "פניה מהאתר";
const MASSAGE_BODY = "היי עליזה. הגעתי אלייך מאתר";
const SITE_URL = "https://alizart-98fed.web.app/";

const Multimedia = () => {
  return (
    <div className="multimedia-strip">
      <div className="talk-to-us"> (: דברו איתנו</div>
      <div className="icons-con">
        <div className="con-icon">
          <a href={`https://wa.me/${PHONE_NUMBER}?text=${MASSAGE_BODY}`}>
            <img alt="whatsapp" src={WHATSAPP_ICON} className="contact-icon" />
          </a>
        </div>
        <TelegramShareButton url={SITE_URL} title={MAIL_SUBJECT}>
          <div className="con-icon">
            <img alt="telegram" src={TELEGRAM_ICON} className="contact-icon" />
          </div>
        </TelegramShareButton>
        <FacebookProvider appId="1228837894161399">
          <div className="con-icon">
            <a
              href={`https://m.me/${PAGE_ID}?fbclid=IwAR1vjDNkRi6nTiHkv9fY7ajGaN7kxogXurgu-Un2YzkU83sx26RUBABauTY`}
            >
              <img
                alt="messenger"
                src={MESSENGER_ICON}
                className="contact-icon"
              />
            </a>
          </div>
        </FacebookProvider>
        <div className="con-icon">
          <a
            href={`mailto:${MAIL}?subject=${MAIL_SUBJECT}&body=${MASSAGE_BODY}`}
          >
            <img alt="mail" src={MAIL_ICON} className="contact-icon" />
          </a>
        </div>
      </div>
    </div>
  );
};

export default Multimedia;
