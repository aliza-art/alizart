import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { app } from "../../firebaseSettings";
import { camelize } from "../../helper/string";
import Loader from "react-loader-spinner";
import "./Gallery.scss";
import classNames from "classnames";

interface AlbumInfo {
  headerName: string;
}
export const albumsMap: { [key: string]: AlbumInfo } = {
  usersUploads: {
    headerName: "תמונות שהעלתם",
  },
  general: {
    headerName: "גלרייה כללית",
  },
};

export const Gallery = () => {
  const [albumsImg, setAlbumsImg] = useState<any>({});
  useEffect(() => {
    Object.keys(albumsMap).forEach((currAlbum) => {
      app
        .database()
        .ref(camelize(currAlbum))
        .orderByChild("uploadDay")
        .limitToLast(1)
        .once("value")
        .then((response) => {
          var a: any = { ...albumsImg };
          var aa: any = Object.values(response.val())[0];
          a[currAlbum] = aa.url;
          setAlbumsImg((s: any) => {
            return { ...s, ...a };
          });
        });
    });
  }, []);

  const galleryClass = classNames("gallery", {
    loader: albumsImg && Object.keys(albumsImg).length === 0,
  });
  return (
    <div className={galleryClass}>
      {Object.keys(albumsImg).length !== 0 ? (
        Object.keys(albumsMap).map((curAlbum) => {
          return (
            <Link to={`/album/${curAlbum}`}>
              <div className="album-link">
                <p>{albumsMap[curAlbum].headerName}</p>
                <img
                  src={albumsImg[curAlbum] ? albumsImg[curAlbum] : ""}
                  alt={curAlbum}
                />
              </div>
            </Link>
          );
        })
      ) : (
        <Loader
          type="Circles"
          color="#d3a5e5"
          height={100}
          width={100}
          timeout={3000} //3 secs
        />
      )}
    </div>
  );
};

export default Gallery;
