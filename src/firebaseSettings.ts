import firebase from "firebase";
import "firebase/storage";

export const app = firebase.initializeApp({
  apiKey: "AIzaSyCHHASTDHPeBnxrZnfSXXwOAQBJCbGuHAE",
  authDomain: "alizart-98fed.firebaseapp.com",
  databaseURL: "https://alizart-98fed.firebaseio.com",
  projectId: "alizart-98fed",
  storageBucket: "alizart-98fed.appspot.com",
  messagingSenderId: "965114101126",
  appId: "1:965114101126:web:a2e38e10a19c1c4e530007",
});
