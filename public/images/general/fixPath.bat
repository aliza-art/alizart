@echo off
setlocal enabledelayedexpansion
set /a counter=1

for  %%f in  (DSC*) do ( 
     ren "%%f" "DSC_!counter!.jpg"
     set /a counter+=1
)

  pause >nul
